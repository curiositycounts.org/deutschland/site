---
layout: default
title: Home
nav_order: 1
description: "Kostenlos lernen."
permalink: /
---
# Wilkommen in CuriosityCounts.org
CuriosityCounts.org ist die kostenlose Lernwebseite für alle.
- **100% kostenlos**: Wir wollen nicht, deine hart verdientes Geld wegzunehmen. Kostenlos lernen, phne Werbung.
- **Interaktive Übungen**: Lerne beim machen. Unsere interaktive Übungen machen Spaß!
- **Sicher**: Wir können Ihre persönlichen Daten nicht sehen. Ohne deine Passwort, sehen wir etwas wie das:
`9QuflCsW7n5kvjwtpmMkmMVGvZ4HcaKYaT9uo89Hc39ZiSsWkIYDhiHNqk19RfifqZ`